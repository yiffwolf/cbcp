#!/usr/bin/env python3
import cbcp
import cbcp.boot
from cbcp.tools import pause
from cbcp.boot import summon_with_message,SUMMON_SELF,SUMMON_PY2
from sys import stdout, stderr

if __name__ == "__main__":
	summon_with_message(
		"- Unpackaging: ",
		SUMMON_SELF,
		"tasks.unpack",
		"__task__",
		stdout=stdout,
		stderr=stderr
	)
	summon_with_message(
		"- Extracting keys: ",
		SUMMON_PY2,
		"tasks.keyextract",
		"__task__",
		stdout=stdout,
		stderr=stderr
	)
	pause()

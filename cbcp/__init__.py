# coding=utf-8
import os

VERSION = "1.0.1"
AUTHORS = ["Yoichi Fan <yme@waifu.club>"]
NAMELEN = 60 # Maximum number of characters a displayed name has
TIMEOUT = 10 # How many seconds to wait for a subprocess

# Directory names
WORK_DIR    = "work/"         # Where the game in unpacked and you edit it
SOURCE_DIR  = "src/"          # Where the original game files go
TARGET_DIR  = "runtime/game/" # Where the finished released files go
KEYS_DIR    = "keys/"         # Where the key files are stored
RENPY_DIR   = "renpy/"        # Where to find the modified Ren'Py library
RUNTIME_DIR = "runtime/"      # Where the Ren'Py runtime is kept
DIFF_DIR    = "diff/"         # Where generated patch files go

# Parallelism
PARALLEL = True
JOBS     = 4

################
### INTERNAL ###
################

STD_DIRS = [
	WORK_DIR,
	SOURCE_DIR,
	TARGET_DIR,
	KEYS_DIR,
	RENPY_DIR,
	RUNTIME_DIR,
	DIFF_DIR
]

DEBUG = {
	"nopanic": False,
}

FEATURES = {
	# Required
	"execnet": True,
	"renpy":   True,
	# Optional
	"bindiff": True,	# 'bsdiff4' to manage binary paatches
	"binhash": True,	# 'hashlib' to validate files
	"pip":     True,	# 'pip' to bootstrap libraries
}

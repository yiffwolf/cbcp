# encoding=utf-8
import zlib
import struct
import pickle

class Rpc2Writer:
	HEADER = "RENPY RPC2".encode("us-ascii")

	def __init__(self, fn):
		self.file = open(fn, "wb")

	def write_header(self):
		# The header of an RCP2 file consists of the string "RENPY RPC2" (here
		# assumed to be encoded as US-ASCII, which oughtta be good enough)
		# followed by an allocaation table.
		#
		# The allocation table is made up of three entries with three 32-bit
		# integer entries each, containing the slot number, the offset at
		# which the data starts in the file and the length of the data,
		# respectively.
		#
		# It is important to note that slot numbers start at one and not at
		# zero as slots numbered zero, from what I could gather, are
		# considered to be empty/null/invalid slots.
		#
		# The three slot limit is hard-coded into the bit of the engine that
		# generates these .rpyc files. I don't really know the reason for
		# this, if there's even one to begin with. However, even though
		# extending the format to handle more than just three slots would
		# be easy to do, the goal of this is to produce files that are
		# bit-exact equal to the ones produced by Ren'Py's own built-in pickler.
		#
		# Another important shortcoming to keep in mind is that the 32-bit
		# unsigned integer is saved by Ren'Py in the pickling machine's
		# *onw* endianess and byte alignment. Which is idiotic, to say the
		# least for a format that's *supposed* to be the one method of
		# distribution for all games that don't wanna bundle their source code.
		# My frustrations aside, because of the compatibility rule, I do the same.
		f = self.file
		f.seek(0, 0)
		f.write(self.HEADER)

		d = struct.pack("III", 0, 0, 0)
		for i in xrange(3):
			f.write(d)

	def write_slot(self, slot, data):
		if not slot in [1, 2, 3]:
			raise IndexError("Only slots 1, 2 and 3 are available in RPC2")

		f = self.file
		f.seek(0, 2)

		offset = f.tell()
		data   = zlib.compress(data, 9)
		length = len(data)
		header = struct.pack("III", slot, offset, length)

		f.seek(len(self.HEADER) + (slot - 1) * len(header) , 0)
		f.write(header)

		f.seek(offset, 0)
		f.write(data)

	def write_md5(self, digest):
		f = self.file
		f.seek(0, 2)
		f.write(digest)


class Rpc2Reader:
	HEADER="RENPY RPC2".encode("us-ascii")
	def __init__(self, fn):
		self.file = open(fn, "rb")

	def slots(self):
		f = self.file
		l = len(struct.pack("III", 0, 0, 0))

		f.seek(len(self.HEADER), 0)
		b = f.read(l * 3)

		s = (struct.unpack("III", b[0:l]), struct.unpack("III", b[l:l*2]), struct.unpack("III", b[2*l:3*l]))
		return s

	def slot(self, n):
		if not n in [1, 2, 3]:
			raise IndexError("Only sections 1, 2 and 3 are supported")

		f = self.file
		for (number, offset, size) in self.slots():
			if number == n:
				f.seek(offset, 0)
				b = f.read(size)
				return zlib.decompress(b)

		return None

class RpaReader:
	def index(self):
		return None

	def offset(self):
		return None

	def key(self):
		return None

	def version(self):
		return None

	def open(self, key):
		return None

	def close(self):
		pass

	@staticmethod
	def new(fname):
		f = open(fname, "rb")
		h = f.read(7).decode("us-ascii")

		f.seek(0, 0)
		if h == "RPA-3.0":
			return Rpa3Reader(f)
		elif h == "RPA-2.0":
			return Rpa2Reader(f)
		else:
			return RpiReader(f)


class Rpa3Reader(RpaReader):
	# Static offset table
	OFFSET_BEG = 8
	OFFSET_LEN = 16
	KEY_BEG = 25
	KEY_LEN = 8

	# Dynamic offset table
	_index  = {}
	_offset = 0
	_key    = 0
	_file   = None

	def __init__(self, f):
		self._file = f
		f.seek(self.OFFSET_BEG, 0)
		offset = f.read(self.OFFSET_LEN).decode("us-ascii")

		f.seek(self.KEY_BEG, 0)
		key = f.read(self.KEY_LEN).decode("us-ascii")

		offset = int(offset, 16)
		key    = int(key, 16)

		# Load the index table from the end of the file, it's assumed, as in
		# the engine's archive loader, that the index starts and offset and
		# ends at EOF. After we load and unpickle it, we deobufscate the offset
		# table in the index using the conveniently placed key.
		f.seek(offset)
		index = f.read()
		self._index_length = len(index)

		index = zlib.decompress(index)
		index = pickle.loads(index)

		for fname in index.keys():
			if len(index[fname][0]) == 2:
				index[fname] = [ (o ^ key, s ^ key) for (o, s) in index[fname] ]
			elif len(index[fname][0]) == 3:
				index[fname] = [ (o ^ key, s ^ key, e) for (o, s, e) in index[fname] ]

		self._index  = index
		self._offset = offset
		self._key    = key

	def index(self):
		return self._index

	def offset(self):
		return self._offset

	def index_length(self):
		return self._index_length

	def key(self):
		return self._key

	def version(self):
		return (3, 0)

	def open(self, key):
		from io import BytesIO

		finfo = self._index[key]
		f = BytesIO()
		for fragment in finfo:
			offset = 0
			length = 0
			start  = None

			if len(fragment) == 2:
				(offset, length) = fragment
			elif len(fragment) == 3:
				(offset, length, start) = fragment

			self._file.seek(offset, 0)
			data = self._file.read(length)

			start = start.encode("utf-8")
			f.write(start)
			f.write(data)

		f.seek(0, 0)
		return f

	def close(self):
		self._file.close()

class Rpa2Reader(RpaReader):
	# RPA 2.0 is the same as RPA 3.0 without obfuscation or keys
	# Static offset table
	OFFSET_BEG = 8
	OFFSET_LEN = 16

	# Dynamic offset table
	_index  = {}
	_offset = 0
	_file   = None

	def __init__(self, f):
		self._file = f
		f.seek(self.OFFSET_BEG, 0)
		offset = f.read(self.OFFSET_LEN).decode("us-ascii")
		offset = int(offset, 16)

		# Load the index table from the end of the file, it's assumed, as in
		# the engine's archive loader, that the index starts and offset and
		# ends at EOF. After we load and unpickle it, we deobufscate the offset
		# table in the index using the conveniently placed key.
		f.seek(offset)
		index = f.read()
		index = zlib.decompress(index)
		index = pickle.loads(index)

		self._index  = index
		self._offset = offset

	def index(self):
		return self._index

	def offset(self):
		return self._offset

	def version(self):
		return (2, 0)

	def close(self):
		self._file.close()


class RpiReader(RpaReader):
	def __init__(self, f):
		raise Exception("Parsing for RPI hasn't been implemented yet")

def fcopy(src, dest, length=None, bufflen = 4096):
	amm = 0
	while True:
		batch = bufflen
		if not length is None:
			batch   = min(length, bufflen)
			length -= batch

		buff = src.read(batch)
		if len(buff) == 0:
			break

		amm += dest.write(buff)
	return amm


class FragmentedFile:
	def __init__(self, rpa, key, frags):
		self._frags  = frags
		self._foffs  = []
		self._key    = key
		self._rpa    = rpa
		self._file   = rpa.get_file()
		self._offset = 0
		self._recalc_offs()

	def length(self):
		total = 0
		for frag in self._frags:
			(offset, length) = frag
			total += length

		return total

	def seek(self, offset, whence=0):
		length = self.length()
		def check(offset):
			if offset > length or offset < 0:
				raise Exception("Tried to seek to invalid offset {}".format(offset))

		if whence == 0:
			# SEEK_SET
			target = offset
		elif whence == 1:
			# SEEK_CUR
			target = self._offset + offset
		elif whence == 2:
			# SEEK_END
			target = length + offset

		check(target)
		self._offset = target


	def _recalc_offs(self):
		off_length = len(self._foffs)
		fra_length = len(self._frags)
		assert off_length <= fra_length

		if fra_length == 0:
			return

		missing = fra_length - off_length
		for slot in range(missing):
			try:
				(pi, roffset) = self._foffs[-1]
				self._foffs.append((off_length - 1 + slot, roffset + self._frags[pi][1]))
			except:
				self._foffs.append((0, 0))

	def _fragsearch(self, offset, fraglist):
		length = len(fraglist)
		if length == 0:
			return (offset, None)
		if length == 1:
			(i, roffset) = fraglist[0]
			frag = self._frags[i]

			if offset >= roffset and offset <= roffset + frag[1]:
				return (roffset, frag)
			else:
				return (offset, None)
		else:
			# Search further
			pivot  = int(length / 2)

			list_a = fraglist[:pivot + 1]
			list_b = fraglist[pivot + 1:]

			(i, roffset) = fraglist[pivot]
			frag = self._frags[i]
			if offset >= roffset and offset < roffset + frag[1]:
				return (roffset, self._frags[i])
			elif roffset > offset:
				assert list_a != fraglist
				return self._fragsearch(offset, list_a)
			else:
				assert list_b != fraglist
				return self._fragsearch(offset, list_b)

	def _fragment(self, offset):
		"""
		Returns the fragment corresponding to the given offset, or None if
		the offset is equal to the length, in a tuple with its relative
		offset in the file.
		"""
		length = self.length()
		if offset > length or offset < 0:
			raise Exception("Tried to get data for invalid offset {}".format(offset))

		foffset = 0
		for frag in self._frags:
			(_, length) = frag

			if foffset + length > offset:
				return (foffset, frag)

			foffset += length
		else:
			return (foffset, None)

	def _merge_frag(self, frag):
		"""
		Merges a fragment into the fragments we already know.
		"""
		# Because the data is stored linearly, we can only stitch stuff that
		# comes right after the last fragment
		for i in range(len(self._frags))[-1:]:
			f = self._frags[i]
			if len(f) == 2:
				(offset, length) = f
				start = None
			elif len(f) == 3:
				(offset, length, start) = f

			a_x0 = frag[0]
			a_x1 = frag[0] + frag[1]
			b_x0 = offset
			b_x1 = offset + length
			if a_x0 < b_x1 and b_x0 < a_x1:
				# A and B overlap
				raise Exception("Tried registering overlapping fragment areas!")
			elif a_x0 == b_x1:
				# A starts right after B
				offset = offset
				length = length + frag[1]
				if start is None:
					self._frags[i] = (offset, length)
				else:
					self._frags[i] = (offset, length, start)

				break
			elif b_x0 == a_x1:
				# B starts right after A
				# SO CLOSE YET SO FAAAAAR
				pass
		else:
			# No significant optimization can be done to the existing
			# fragment layout, all fragments are disjointed
			self._frags.append(frag)

		self._recalc_offs()

	def write(self, data):
		written = 0
		while len(data) > 0:
			# Figure out how much of the data we can write in the current fragment
			rela_offset, fragment = self._fragment(self._offset)
			if fragment is None:
				# We're at the end of the file, ask the writer to allocate some
				# new fragments to fit the data we're being asked to write
				for frag in self._rpa.alloc_fragments(self._key, len(data)):
					self._merge_frag(frag)

				rela_offset, fragment = self._fragment(self._offset)

			init_offset = self._offset - rela_offset
			frag_offset, frag_length = fragment
			offset = frag_offset + init_offset
			length = frag_length - init_offset

			target = data[:length]
			data = data[length:]

			self._file.seek(offset, 0)
			written += self._file.write(target)

			self._offset += len(target)

		return written


class Rpa3Writer:
	OFFSET_BEG = 8
	OFFSET_LEN = 16
	KEY_BEG    = 25
	KEY_LEN    = 8
	HEADER     = "RPA-3.0 {:016x} {:08x}\nMade with Ren'Py."
	NIL_HEADER = HEADER.format(0, 0)

	_index   = {}
	_key     = 0
	_outf    = None
	_orphans = []

	def __init__(self, fname, mode="a"):
		# Iitialize default parameters
		self._index   = {}
		self._key     = 0xDeadBeef
		self._files   = []
		self._orphans = []

		if mode == "w":
			# We're writing a new file
			self._outf = open(fname, "wb")
			self._outf.write(self.NIL_HEADER.encode("us-ascii"))
		elif mode == "a":
			# We're adding to an existing file, so we'll need to figure out
			# what the existing index data is, and then load it in, then mark
			# the origina index data in the file as an orphaned fragment, that
			# can be reused later for new data.
			self._outf = open(fname, "r+b")
			self._outf.seek(0, 0)
			reader = Rpa3Reader(self._outf)

			self._index = reader.index()
			self._key   = reader.key()

			self._orphans.append((reader.offset(), reader.index_length()))

	def set_key(self, key):
		self._key = key

	def get_file(self):
		return self._outf

	def index(self):
		return self._index

	def in_index(self, key):
		return key in self._index

	def get_orphans(self):
		return self._orphans

	def push_fragment(self, f, key, length, offset=None, start=None):
		"""
		Appends a new fragment to the end of the file, registering it as
		belonging to the given key, returning how many bytes were written.
		"""
		self._outf.seek(0, 2)

		roffset = self._outf.tell()
		if (not offset is None) and roffset != offset and roffset < offset:
			# Pad until we get to the actual expected offset, then mark the
			# padded region as vacant
			self._outf.write(bytearray(offset - roffset))
			self._orphans.append((roffset, offset - roffset))
			roffset = self._outf.tell()


		written = fcopy(f, self._outf, length=length)
		if written < length:
			# Pad out if the file's too short
			self._orphans.append((roffset + written, length - written))
			written += self._outf.write(bytearray(length - written))

		if start is None or len(start) == 0:
			fragment = (roffset, written)
		else:
			fragment = (roffset, written, start)

		if key in self._index:
			for f in self._index[key]:
				if len(f) > 2 or len(fragment) > 2:
					raise Exception(
						"Ren'Py does not support loading fragmented files with a start parameter"
					)

			self._index[key].append(fragment)
		else:
			self._index[key] = [fragment]

		return written

	def rewrite_fragment(self, f, key, index):
		"""
		Overwrites data in a fragment with given index belonging to the given
		key, returning how many bytes were written. This function will write
		at most the original length of the fragment, if you want to write more,
		you will have to overwrite other fragments or append new ones.
		"""
		frags = self._index[key]
		frag  = frags[index]

		if len(frag) == 2:
			(offset, length) = frag
			start = None
		elif len(frag) == 3:
			(offset, length, start) = frag
		else:
			raise Exception("Corrupt fragment for key '{}', index {}".format(key, index))

		self._outf.seek(offset, 0)
		written = self._outf.write(f.read(length))

		if written < length:
			# Mark the remaninder of the old fragment as orphaned, so we can
			# reuse it later
			self._orphans.append((offset + written, length - written))

		if start is None:
			self._index[key] = (offset, written, start)
		else:
			self._index[key] = (offset, written)

		return written

	def get_fragments(self, key):
		return self._index[key]

	def shrink(self, key, length):
		foff = 0
		for i in range(len(self._index[key])):
			j = i
			if foff + self._index[key][i][1] >= length:
				break

			foff += self._index[key][i][1]

		# Drop all of the fragments after the length cutoff
		for i in range(j + 1, len(self._index[key])):
			self._orphans.append(self._index[key].pop(i))

		# Drop the unused part of the last fragment
		self._index[key][j] = (self._index[key][j][0], length - foff)
		self._orphans.append((self._index[key][j][0] + length - foff, self._index[key][j][1] - length + foff))

	def alloc_fragments(self, key, length):
		"""
		Allocate new fragments to fit length bytes of data, more than one
		fragment may be returned, as orphans are reused in this process
		"""
		remaining = length
		frags = []
		while remaining > 0:
			if len(self._orphans) > 0:
				frag = self._orphans.pop(0)
			else:
				frag = None

			if frag is None:
				# Allocate a new fragment at the end
				self._outf.seek(0, 2)
				offset = self._outf.tell()
				self._outf.write(bytearray(remaining))
				frag = (offset, remaining)
			elif frag[1] > remaining:
				# We don't need the whole orphan, take just what we need and
				# re-register the unused area as orphaned
				self._orphans.insert(0, (frag[0] + remaining, frag[1] - remaining))
				frag = (frag[0], remaining)

			# Register the fragment
			if key in self._index:
				for i in range(len(self._index[key]))[-1:]:
					f = self._index[key][i]
					if len(f) == 2:
						(offset, length) = f
						start = None
					elif len(f) == 3:
						(offset, length, start) = f

					a_x0 = frag[0]
					a_x1 = frag[0] + frag[1]
					b_x0 = offset
					b_x1 = offset + length
					if a_x0 < b_x1 and b_x0 < a_x1:
						# A and B overlap
						raise Exception("Tried registering overlapping fragment areas!")
					elif a_x0 == b_x1:
						# A starts right after B
						offset = offset
						length = length + frag[1]
						if start is None:
							self._index[key][i] = (offset, length)
						else:
							self._index[key][i] = (offset, length, start)

						break
					elif b_x0 == a_x1:
						# B starts right after A
						# SO CLOSE YET SO FAAAAAAAR
						pass
				else:
					# No significant optimization can be done to the existing
					# fragment layout, all fragments are disjointed
					self._index[key].append(frag)
			else:
				# New key!
				self._index[key] = [frag]

			remaining -= frag[1]
			frags.append(frag)

		return frags


	def open(self, key):
		if key in self._index:
			frags = self._index[key]
		else:
			frags = []

		return FragmentedFile(self, key, frags)

	def delete(self, key):
		"""
		Deletes a file from the archive
		"""
		for frag in self._index[key]:
			if len(frag) == 2:
				(offset, length) = frag
				start = None
			elif len(frag) == 3:
				(offset, length, start) = frag
			else:
				raise Exception("Corrupt fragment for key '{}', index {}".format(ket, index))

			self._orphans.append((offset, length))

		self._index.pop(key, None)

	def commit(self):
		# Obfuscate the index using our key
		index = {}
		for key in self._index.keys():
			if len(self._index[key]) == 0:
				continue

			if len(self._index[key][0]) == 3:
				index[key] = [(o ^ self._key, s ^ self._key, n) for (o, s, n) in self._index[key]]
			elif len(self._index[key][0]) == 2:
				index[key] = [(o ^ self._key, s ^ self._key) for (o, s) in self._index[key]]


		# Commit the index
		self._outf.seek(0, 2)

		offset = self._outf.tell()
		index  = pickle.dumps(index, 2)
		index  = zlib.compress(index, 9)
		self._outf.write(index)

		# Write the actual header
		self._outf.seek(0, 0)
		self._outf.write(self.HEADER.format(offset, self._key).encode("us-ascii"))



def bootstrap():
	# This mimicks Ren'Py's own bootstrap() function. Which is required to be
	# run in order for the tools in the engine to work properly. This is a very
	# light version of the one in the engine, though. We only do the least
	# ammount of housekeeping in order to get it to work.
	import renpy
	renpy.config.renpy_base = "renpy/"
	renpy.config.basedir    = "."
	renpy.config.gamedir    = "work/"
	renpy.config.args       = []
	renpy.config.logdir     = "."
	renpy.config.searchpath = []
	renpy.config.layers     = [ "master", "transient", "screens", "overlay" ]
	renpy.config.top_layers = []
	renpy.config.transient_layers     = [ "transient" ]
	renpy.config.overlay_layers       = [ "overlay" ]
	renpy.config.context_clear_layers = [ "screens" ]
	renpy.config.statement_callbacks  = []

	renpy.sl2.slparser.init()
	renpy.game.script = renpy.script.Script()
	renpy.game.log    = renpy.python.RollbackLog()

	import sys
	renpy.python.create_store("store")
	renpy.store = sys.modules["store"]
	renpy.exports.store = renpy.store

	import subprocess
	sys.modules["renpy.store"]      = sys.modules["store"]
	sys.modules["renpy.subprocess"] = subprocess

	for (key, val) in renpy.defaultstore.__dict__.iteritems():
		#renpy.store.__dict__.setdefault(key, val)
		renpy.store.__dict__[key] = val
	renpy.store.eval = renpy.defaultstore.eval

	for (key, val) in renpy.__dict__.iteritems():
		vars(renpy.exports).setdefault(key, val)

	renpy.game.contexts = [ renpy.execution.Context(True) ]
	renpy.game.contexts[0].early_run = True

# Load the common files and finish setting up the parser
def load_commons():
	import os
	import cbcp
	import renpy
	comdirn = os.path.join(cbcp.RENPY_DIR, "common/")
	if not os.path.exists(comdirn):
		raise Exception("Cannot find common directory {}".format(comdirn))

	commons = []
	for f in os.listdir(comdirn):
		if f.endswith(".rpy"):
			commons.append(f)

	commons = sorted(commons)
	for fname in commons:
		fpath = os.path.join(comdirn, fname)
		stree = renpy.parser.parse(fpath)
		if stree == None:
			raise Exception("Failed to load common file {}".format(fpath))

		renpy.game.script.update_bytecode()
		for node in stree:
			if isinstance(node, renpy.ast.EarlyPython):
				node.early_execute()
				node.execute()

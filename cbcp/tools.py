import os
import cbcp
import sys

#########################
### UTILITY FUNCTIONS ###
#########################

def strbool(string):
	return string.lower() in ["yes", "on", "true", "t", "1"]


def pause(message="== Press return to continue =="):
	sys.stdout.write("{}".format(message))
	sys.stdout.flush()

	from sys import stdin
	_ = stdin.readline()

def panic(message):
	from sys import exit
	print("==========================")
	print("A fatal error has occured:")
	print(message)
	print("==========================")
	if not cbcp.DEBUG["nopanic"]:
		pause()
		exit(1)
	else: print("!!! CONTINUING")

def prompt(message, default=False):
	if default == True: hint = "[Y/n]"
	else: hint = "[y/N]"

	sys.stdout.write("{} {}".format(message, hint))
	sys.stdout.flush()
	from sys import stdin
	for answer in stdin:
		if answer.strip() == "":
			return default
		elif answer.strip() == "Y" or answer.strip() == "y":
			return True
		elif answer.strip() == "N" or answer.strip() == "n":
			return False
		sys.stdout.write("{} {}".format(message, hint))
		sys.stdout.flush()

def test_file(fname, sha1):
	try:
		f = open(fname, "r")
		h = hashlib.new("sha1")
		h.update(f.read())
		return h.hexdigest() == sha1
	except Exception as e:
		print("\t* Test failed for file %s" % fname)
		print(e)
		return False

def test_cmd(cmd, timeout=10):
	try:
		from subprocess import Popen,PIPE
		cmd = Popen(
			cmd,
			stdin=None,
			stdout=PIPE,
			stderr=PIPE)
		cmd.wait(timeout)
		# Assume the command succeeded
		return (True, cmd.returncode)
	except Exception as e:
		print("\t* Test failed for command %s" % cmd)
		print(e)
		return (False, 0)

def hashload(f, hasher, buffsize=4096):
	while True:
		buff = f.read(buffsize)
		if len(buff) == 0:
			break
		hasher.update(buff)

def fcopy(src, target, buffsize=65536):
	written = 0
	while True:
		a = src.read(buffsize)
		if len(a) == 0:
			break

		written += target.write(a)
	return written

def stat_newer(sa, sb, bias_new=False):
	if bias_new:
		return sa.st_ctime > sb.st_ctime or sa.st_atime > sb.st_atime or sa.st_mtime > sb.st_mtime
	else:
		return sa.st_ctime > sb.st_ctime and sa.st_atime > sb.st_atime and sa.st_mtime > sb.st_mtime

def fnewer(a, b, bias_new=False):
	"""
	Returns whether the file a is newer than the file b.

	bias_new chooses whether the bias is towards a resulting
	new diagnosis, only needing one file timestap to be newer in
	file a for it to be considered newer than file b.
	If bias_new is False, all file timestamps need to be newer in
	file a in order for it to be considered newer than file b.
	"""
	import os
	sa = os.stat(a)
	sb = os.stat(b)
	return stat_newer(a, b, bias_new)

def ensure_dirs(dirns):
	for dirn in dirns:
		if not os.path.exists(dirn):
			os.makedirs(dirn)

def ensure_std_dirs():
	ensure_dirs(cbcp.STD_DIRS)

def sfname(fname):
	import os
	return os.path.join(SOURCE_DIR, fname)

def kfname(fname):
	import os
	return os.path.join()

#!/usr/bin/env python2
# encoding=utf-8
# This will have an rpyc dissassembler.
# Eventually.

import renpy
from cbcp.rpyutils import bootstrap,load_commons

class UnknownReverseError(Exception):
	pass

class HandlerNode(dict):
	key = None
	fun = None

	def __init__(self, key, fun):
		self.key = key
		self.fun = fun
	
	def __getitem__(self, key):
		if key == None:
			return self
		else:
			return dict(self)[key]
	
	def __call__(self, node):
		self.fun(node)
	
	def has(self, key)
		return key is None or key in dict(self)

class HandlerGraph:
	root = None

	def __init__(self):
		self.root = HandlerNode(None, None)

	def reg(self, path=None):
		if not isinstance(path, (tuple, list)):
			path = [path]

		def decorate(fn):
			node = self.root
			while len(path) > 0:
				bit = path.pop(0)

				if not node.has(bit):
					node[bit] = HandlerNode(bit, None)
				node = node[bit]

			node.fun = fn

		return decorate

	def handlers(self, key):
		if key is None:
			if root.fun is None:
				return []
			else:
				return [root.fun]

		h = []
		node = self.root
		if not node.fun is None:
			h.append(node.fun)

		while key in node:
			node = node[key]
			if not node.fun is None:
				h.append(node.fun)
		
class 

class Support:
	# Converts an imspec to the full 7-position format:
	# (name, expression, tag, at_list, layer, zorder, behind)
	#  name       - Name of the image to be displayed
	#  expression - Used for `show expression`
	#  tag        - ???
	#  at_list    - Expression list (after `at`)
	#  layer      - Layer name (after `onlayer`)
	#  zorder     - ???
	#  behind     - List of labels for characters (after `behind`)
	@staticmethod
	def fat_imspec(imspec):
		if len(imspec) == 7:
			return imspec
		elif len(imspec) == 6:
			a, b, c, d, e, f = imspec
			return (a, b, c, d, e, f, [])
		elif len(imspec) == 3:
			a, b, c = imspec
			return (a, b, c, None, None, [])


class Dissassembler:
	handlers = HandlerGraph()
	def __init__():
		pass

	@handlers.reg()
	def d_Any(node):
		try:
			return node.get_code()
		except:
			# Well nice try I guess
			return None
	
	@handlers.reg(renpy.ast.Label)
	def d_Label(node):
		return "label {}:".format(node.name)
	
	@handlers.reg(renpy.ast.Python)
	def d_Python(node):
		return "$ {}".format(node.code.source)
	
	@handlers.reg(renpy.ast.Say)
	def d_Say(node):
		return "{} \"{}\"".format(node.who, node.what)	
	
	@handlers.reg(renpy.ast.Scene)
	def d_Scene(node):
		im = Support.fat_imspec(node.imspec)
		la = node.layer

		return "scene {} {}".format(im[0][0], im[0][1])
	
	@handlers.reg(renpy.ast.Show)
	def d_Show(node):
		im = Support.fat_imspec(node.imspec)
		return "# show ??"
	

#!/usr/bin/env python3
# coding=utf-8
import cbcp
from cbcp import FEATURES,DEBUG
from cbcp.tools import prompt,test_cmd,panic,ensure_std_dirs

#########################
### VERSION BOOTSTRAP ###
#########################

import sys
SUMMON_SELF=sys.executable

if SUMMON_SELF == None or SUMMON_SELF == "":
	panic("Cannot obtain the name of the python interpreter")

# Figure out where our other snakes are
import os

SUMMON_PY2=None
SUMMON_PY3=None

def _py_ver(fpath):
	ver_cmd = [
		fpath,
		"-c",
		"from sys import version_info as v; print('{},{}'.format(v.major, v.minor))"
	]

	import subprocess as sp
	p = sp.Popen(
		ver_cmd,
		stdin=sp.PIPE,
		stdout=sp.PIPE,
		stderr=sp.PIPE
	)

	POLLS = 100
	import time
	for i in range(POLLS):
		if not p.poll() is None:
			break

		t = float(cbcp.TIMEOUT) / float(POLLS)
		time.sleep(t)
	else:
		raise TimeoutException("Subprocess timed out")

	out = p.stdout.readline()
	out = out.decode("us-ascii").strip().split(",")
	out = [int(x) for x in out]

	return out


if "SUMMON_PY2" in os.environ:
	fpath = os.environ["SUMMON_PY2"]
	try:
		out = _py_ver(fpath)
		if out[0] == 2 and out[1] >= 7:
			SUMMON_PY2 = fpath
	except Exception as e:
		print("Invalid Python2 in SUMMON_PY2: {}".format(e))
		SUMMON_PY2 = None

if "SUMMON_PY3" in os.environ:
	fpath = os.environ["SUMMON_PY3"]
	try:
		out = _py_ver(fpath)
		if out[0] == 3 and out[1] >= 5:
			SUMMON_PY3 = fpath
	except Exception as e:
		print("Invalid Python3 in SUMMON_PY3: {}".format(e))
		SUMMON_PY3 = None

path = None
try:
	path = os.get_exec_path()
except:
	# We might not be able to use os.get_exec_path() just yet. If that's the
	# case, try a sort of best possible guess on a per-platform basis for the
	# exec path. This happens when Python 2 in cbcp.boot
	if path is None:
		n = os.name
		if n == "posix":
			# UNIX platform and alikes
			if "PATH" in os.environ:
				env  = os.environ["PATH"]
				path = env.split(":")
			else:
				path = []
		elif n == "nt":
			# Windows and ReactOS
			if "Path" in os.environ:
				env  = os.environ["Path"]
				path = env.split(";")
			else:
				path = []
		else:
			# 'riscos', 'java', 'ce', 'os2'
			# We don't know how to deal with these platforms
			path = []

for dirn in path:
	try:
		_ = os.stat(dirn)
	except:
		continue

	for fname in os.listdir(dirn):
		fpath = os.path.join(dirn, fname)
		if not os.path.isfile(fpath):
			continue
		if not ("python" in fname and not "pythonw" in fname):
			continue

		# Try executing and figure out what version we have
		try:
			out = _py_ver(fpath)

			if out[0] == 2 and out[1] >= 7 and SUMMON_PY2 is None:
				SUMMON_PY2 = fpath
			elif out[0] == 3 and out[1] >= 5 and SUMMON_PY3 is None:
				SUMMON_PY3 = fpath
		except:
			# Invalid python candidate
			pass

if SUMMON_PY3 is None:
	panic("Cannot find Python 3.5 or newer, make sure it's installed correctly")

if SUMMON_PY2 is None:
	panic("Cannot find Python 2.7 or newer, make sure it's installed correctly")

# Version gate.
def py3_reboot():
	ma = sys.version_info.major
	mi = sys.version_info.minor
	print("> The script is running under Python {}.{}".format(ma, mi))
	print("> It requires Python 3.5+, which was found")
	print("> Restarting the script using the known Python 3")
	print("")

	args = [
		SUMMON_PY3
	]
	args.extend(sys.argv)

	import subprocess
	sp = subprocess.Popen(args, stdin=None, stdout=None, stderr=None)
	sp.wait()
	sys.exit(sp.returncode)

if sys.version_info.major != 3:
	py3_reboot()
elif sys.version_info.minor < 5:
	py3_reboot()

# SUMMON_SELF should be good enough as our generic Python 3 after this point
SUMMON_PY3 = SUMMON_SELF

####################
### BOOT MESSAGE ###
####################

ensure_std_dirs()
print("cbcp - Camp Buddy Coder's Pack v{}".format(cbcp.VERSION))

_a = ""
for name in cbcp.AUTHORS:
	_a += name + ", "
if len(_a) > 0:
	_a = _a[:len(_a) - 2]

print("by {}".format(_a))
print("")
print("Python 3 is: {}".format(SUMMON_PY3))
print("Python 2 is: {}".format(SUMMON_PY2))
print("")

#######################
### DEBUG BOOTSTRAP ###
#######################

import os
if "CBCP_NOPANIC" in os.environ and strbool(os.environ["CBCP_NOPANIC"]):
	print("!!! NOPANIC MODE IS ENABLED")
	print("!!! ONLY DO THIS IF YOU KNOW WHAT YOU'RE DOING")
	print("!!! REMEMBER TO DISABLE CBCP_NOPANIC WHEN YOU'RE DONE")
	print("")
	DEBUG["nopanic"] = True

#########################
### FEATURE BOOTSTRAP ###
#########################

# Figure out what tools we have at our disposal
def check_features(show=True):
	(pip_test, pip_code) = test_cmd([SUMMON_SELF, "-m", "pip"])
	if not pip_test or pip_code != 0:
		if show: print("- Warning: No 'pip' module found. Cannot bootstrap libraries")
		FEATURES["pip"] = False
	else:
		FEATURES["pip"] = True

	try:
		import bsdiff4 as diff
		global diff
		FEATURES["bindiff"] = True
	except ImportError:
		if show: print("- Warning: No 'bsdiff4' module found. Binary patching is disabled")
		FEATURES["bindiff"] = False

	try:
		import hashlib
		global hashlib
		FEATURES["binhash"] = True
	except ImportError:
		if show: print("- Warning: No 'hashlib' module found. Binary validation is disabled")
		FEATURES["binhash"] = False

	try:
		import execnet
		global execnet
		FEATURES["execnet"] = True
	except ImportError as e:
		if show: print("- Error: Cannot import execnet: %s" % e)
		FEATURES["execnet"] = False

	return FEATURES["execnet"]

def setup_features():
	if not FEATURES["execnet"]:
		if not FEATURES["pip"]:
			panic("Cannot install missing modules because 'pip' is not installed")
		import subprocess
		pip = subprocess.Popen([SUMMON_SELF, "-m", "pip", "install", "--user", "execnet==1.6.0"])
		pip.wait()

if not check_features():
	print("- Incomplete setup detected:")
	if not FEATURES["execnet"]: print("\t* Missing required module 'execnet'")

	if prompt("- Try to install missing components?", default=True):
		setup_features()
		print("- Revalidating")
		if not check_features(show=False):
			panic("Installation failed")
		print("\t* Valid")
	else:
		from sys import exit
		print("- Cancelled")
		exit(1)

############
### GLUE ###
############
def summon(version, module, function, args=[], stdout=None, stderr=None):
	import execnet
	gate = execnet.makegateway("popen//python={}".format(version))
	chan = gate.remote_exec("""
		import sys
		stdout = channel.gateway.newchannel()
		stderr = channel.gateway.newchannel()

		sys.stdout = stdout.makefile("w")
		sys.stderr = stderr.makefile("w")

		channel.send(stdout)
		channel.send(stderr)

		#sys.path.insert(0, "renpy/")
		#sys.path.insert(0, "renpy/imports")

		from {} import {} as fn
		channel.send(fn(*channel.receive()))
	""".format(module, function))

	_stdout = chan.receive()
	_stderr = chan.receive()
	_stdout.setcallback(lambda data: stdout.write(data))
	_stderr.setcallback(lambda data: stderr.write(data))

	chan.send(args)
	return chan.receive()


def summon_with_message(message, version, module, function, args=[], stdout=None, stderr=None):
	if stdout is None and stderr is None:
		import sys
		sys.stdout.write(message)
		sys.stdout.flush()
		if summon(version, module, function, args, stdout, stderr):
			print("Ok")
			return True
		else:
			print("Failed")
			return False
	else:
		print(message)
		return summon(version, module, function, args, stdout, stderr)

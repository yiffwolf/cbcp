#!/usr/bin/env python3
import cbcp
import cbcp.boot
from cbcp.tools import pause
from cbcp.boot import summon_with_message,SUMMON_SELF

if __name__ == "__main__":
	from sys import stdout,stderr
	summon_with_message(
		"- Creating patch files: ",
		SUMMON_SELF,
		"tasks.diff",
		"__task__",
		stdout=stdout,
		stderr=stderr
	)
	pause()

#!/usr/bin/env python3
# coding=utf-8
import json
import zipfile

HEADER_KEY   = "header.json"
FRAGMENT_KEY = "frags/{}"

def hashload(f, hasher, buffsize=4096):
	while True:
		buff = f.read(buffsize)
		if len(buff) == 0:
			break
		hasher.update(buff)


class MissingFileError(Exception):
	pass

class MissingHeaderError(MissingFileError):
	def __init__(self):
		super.__init__("Missing header file {}".format(HEADER_KEY))

class MissingFragmentError(MissingFileError):
	def __init__(self, frag_index):
		super.__init__("Missing fragment file {}".format(FRAGMENT_KEY.format(frag_index)))
		self.index = frag_index

	def index(self):
		return self.index

class InvalidFileError(Exception):
	pass

class InvalidHeaderError(InvalidFileError):
	def __init__(self):
		super.__init__("Invalid header file {}".format(HEADER_KEY))

class InvalidFragmentError(InvalidFileError):
	def __init__(self, frag_index):
		super.__init__("Invalid fragment file {}".format(FRAGMENT_KEY.format(frag_index)))
		self.frag_index = frag_index

	def index(self):
		return self.frag_index

class ChecksumError(Exception):
	checksum = ""
	expected = ""
	def __init__(self, checksum, expected, message):
		super.__init__(message)
		self.checksum = checksum
		self.expected = expected


class OldFileChecksumError(ChecksumError):
	def __init__(self, checksum, expected):
		super.__init__(checksum, expected, "Checksum failed for old file")

class FragmentChecksumError(ChecksumError):
	def __init__(self, frag_index, checksum, expected):
		super.__init__(checksum, expected, "Checksum failed for fragment {}".format(frag_index))
		self.frag_index = frag_index

	def index(self):
		return self.frag_index

class Fd4Writer(zipfile.ZipFile):
	frags = []
	src_hash = None

	def __init__(self, fname):
		super().__init__(fname, mode="w")
		self.frags = []
		self.src_hash = None

	def push_fragment(self, src_length, patch):
		import hashlib
		h = hashlib.sha3_224()
		h.update(patch)
		h = h.hexdigest()
		frag = {"length": src_length, "sha3-224": h}

		# Add the patch to the format
		f = self.open(FRAGMENT_KEY.format(len(self.frags)), "w")
		f.write(patch)
		f.close()

		self.frags.append(frag)

	def set_source(self, f):
		f.seek(0, f.SEEK_SET)

		import hashlib
		h = hashlib.sha3_224()
		hashload(f, h)
		self.src_hash = h.hexdigest()

	def commit(self):
		header = {"fragments": self.frags}
		if not self.src_hash is None:
			header["sha3-224"] = self.src_hash

		h = self.open(HEADER_KEY, "w")
		json.dump(h)
		h.close()


class Fd4Reader(zipfile.ZipFile):
	def __init__(fname):
		super.__init__(fname, mode="r")

		try:
			header = self.open(HEADER_KEY, "r")
			self.header = json.load(header)
			header.close()

			if not "fragments" in self.header:
				self.header["fragments"] = []

		except KeyError as what:
			raise MissingHeaderError() from what
		except JSONDecodeError as what:
			raise InvalidHeaderError() from what

	def old_hash(self):
		if "sha3-224" in self.header:
			return self.header["sha3-224"]
		else:
			return None

	def fragcount(self):
		return len(self.header["fragments"])

	def fraginfo(self, index):
		return self.header["fragments"][index]

	def fragcheck(self, index):
		frag = self.fraginfo(index)
		if not "length" in frag:
			raise InvalidFragmentException(index)

		try:
			pf = self.open(FRAGMENT_KEY.format(index), "rb")
			p  = patch.read()
			pf.close()
		except KeyError as what:
			raise MissingFragmentError(i)

		if "sha3-224" in fragment:
			import hashlib
			h = hashlib.sha3_224()
			h.update(p)
			d = h.hexdigest()
			if d != fragment["sha3-224"]:
				raise FragmentChecksumError(index, d, fragment["sha3-224"])
		else:
			raise FragmentChecksumError(index, None, None)

		return p

	def oldcheck(self, old):
		pos = old.tell()

		oh = self.old_hash()
		if not oh is None:
			import hashlib
			h = hashlib.sha3_224()
			hashload(old, h)
			d = h.hexdigest()
			if d != oh:
				raise OldFileChecksumError(d, oh)
		else:
			raise OldFileChecksumError(None, None)

		old.seek(pos, old.SEEK_SET)

	def apply(self, fold, fout, check_patches=False, check_source=False, check_output=False):
		import sys
		import bsdiff4

		def say(mask, message, f=sys.stdout):
			if verbosity & mask:
				print(message)

		if check_source:
			self.checkold(fold)

		fold.seek(0, f.SEEK_SET)
		for i in range(self.fragcount()):
			frag = self.fraginfo(i)

			# Load the patch into memory
			if check_patches:
				patch = self.fragcheck(i)
			else:
				try:
					patch = self.open(FRAGMENT_KEY.format(i), "rb")
					patch = patch.read()
				except KeyError as what:
					i += 1
					continue

			# Load the file section into memory
			if not "length" in fragment:
				i += 1
				continue

			length = fragment["length"]
			source = fold.read(length)

			if len(source) != length:
				# This might be an error or we might just wanna ignore this,
				# honestly. Depends on whether I decide length to be the
				# expected length of the source fragment. We'll see.
				# TODO: Figure out exactly what length means here.
				pass


			try:
				result = bsdiff4.patch(source, patch)
				fold.write(result)
			except ValueError as what:
				raise InvalidFragmentError(i) from what

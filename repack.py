#!/usr/bin/env python3
import cbcp
import cbcp.boot
from sys import stdout, stderr
from cbcp.tools import pause
from cbcp.boot import summon_with_message,SUMMON_PY2,SUMMON_SELF

if __name__ == "__main__":
	summon_with_message(
		"- Building: ",
		SUMMON_PY2,
		"tasks.build",
		"__task__",
		stdout=stdout,
		stderr=stderr
	)
	summon_with_message(
		"- Repackaging: ",
		SUMMON_SELF,
		"tasks.repack",
		"__task__",
		stdout=stdout,
		stderr=stderr
	)
	pause()

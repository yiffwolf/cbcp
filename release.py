#!/usr/bin/env python3
import cbcp
import cbcp.boot
from cbcp.tools import pause
from cbcp.boot import summon_with_message,SUMMON_SELF,SUMMON_PY2

if __name__ == "__main__":
	from sys import stdout,stderr
	summon_with_message(
		"- Cleaning game distribution: ",
		SUMMON_SELF,
		"tasks.distclean",
		"__task__",
		stdout=stdout,
		stderr=stderr,
	)
	summon_with_message(
		"- Building: ",
		SUMMON_PY2,
		"tasks.build",
		"__task__",
		stdout=stdout,
		stderr=stderr,
	)
	summon_with_message(
		"- Cleaning targets: ",
		SUMMON_SELF,
		"tasks.targetclean",
		"__task__",
		stdout=stdout,
		stderr=stderr,
	)
	summon_with_message(
		"- Repackaging: ",
		SUMMON_SELF,
		"tasks.repack",
		"__task__",
		stdout=stdout,
		stderr=stderr
	)
	pause()

#!/usr/bin/env python3
import cbcp
import cbcp.boot
from cbcp.tools import pause
from cbcp.boot import summon_with_message,SUMMON_PY2,SUMMON_SELF

if __name__ == "__main__":
	from sys import stdout, stderr
	summon_with_message(
		"- Cleaning build files: ",
		SUMMON_SELF,
		"tasks.distclean",
		"__task__",
		stdout=stdout,
		stderr=stderr
	)
	summon_with_message(
		"- Building sources: ",
		SUMMON_PY2,
		"tasks.build",
		"__task__",
		stdout=stdout,
		stderr=stderr
	)
	pause()

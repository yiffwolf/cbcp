# encoding=utf-8
vc_version       = 0
version_tuple    = (7, 3, 2, vc_version)
version_only     = "7.3.2.0"
version          = "Not really Ren'Py 7.3.2.0"
version_name     = "The world (wide web) is plenty."
bytecode_version = 1
savegame_suffix  = ".FUCKYOU"
script_version   = 5004000
store            = None

windows    = False
macintosh  = False
linux      = False
android    = False
ios        = False
emscripten = False


import renpy.log
import renpy.arguments
import renpy.object
import renpy.pydict
import renpy.color
import renpy.audio
import renpy.python
import renpy.audio.audio
import renpy.audio.music
import renpy.audio.renpysound
import renpy.pyanalysis
import renpy.config
import renpy.scriptedit
import renpy.display
import renpy.display.core
import renpy.display.layout
import renpy.game
import renpy.preferences
import renpy.text
import renpy.text.text
import renpy.substitutions
import renpy.display.behavior
import renpy.sl2
import renpy.parser
import renpy.ast
import renpy.atl
import renpy.display.transform
import renpy.display.motion
import renpy.display.viewport
import renpy.display.dragdrop
import renpy.display.im
import renpy.display.imagelike
import renpy.display.particle
import renpy.display.transition
import renpy.display.movetransition
import renpy.add_from
import renpy.curry
import renpy.ui
import renpy.six
import renpy.loadsave
import renpy.sl2.slparser
import renpy.sl2.sldisplayables
import renpy.script
import renpy.statements
import renpy.minstore
import renpy.defaultstore

atl_warper = renpy.atl.atl_warper

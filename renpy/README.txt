THIS IS A MODIFIED VERSION OF THE RENPY ENGINE, STRIPPED OUT OF ALL OF ITS
DEPENDENCIES TO C AND PYGAME, USED FOR BUILD AND ITS RELATED TASKS.  I HAVE
NO IDEA HOW THIS FARES WITH ANY UNTESTED GAME AND IT'S VERY LIKELY TO BREAK.
AND I HOPE TO REPLACE THIS WITH A BETTER SOLUTION IN THE FUTURE, WHATEVER IT
MIGHT BE.

I CLAIM NO OWNERSHIP OVER THESE FILES, EXCEPT THOSE I'VE WRITTEN FROM SCRATCH,
WHICH ARE FEW AND FAR BETWEEN, AND CAN BE FOUND OUT BY DIFF-ING THE "renpy/"
FOLDER WITH THE OFFICAL RENPY 7.3.2'S OWN "renpy/" FOLDER.

RENPY AND ITS ORIGINAL FILES ARE AVAILABLE UNDER THE FOLLOWING LICENCE:
# Copyright 2004-2019 Tom Rothamel <pytom@bishoujo.us>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

ANY FILES MADE BY ME FROM SCRATCH ARE AVAILABLE UNDER THE FOLLOWING LICENCE:
# Copyright 2019 Yoichi Fan <yme@waifu.club>
#
# DO WHAT THE FUCK YOU WANT TO BUT IT'S NOT MY FAULT PUBLIC LICENSE TERMS AND
# CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION:
#     0. You just DO WHAT THE FUCK YOU WANT TO.
#     1. Do not hold the author(s), creator(s), developer(s) or distributor(s)
#        liable for anything that happens or goes wrong with your use of the
#        work.

# coding=utf-8

def ssort(list):
	# Selection sorts a small list
	for i in xrange(len(list)):
		min_v = list[i]
		min_i = i
		for j in xrange(i, len(list)):
			if list[j] < min_v:
				min_i = j
				min_v = list[j]

		if min_i != i:
			list[i], list[min_i] = list[min_i], list[i]

def qsort(list):
	# Quicksorts a list
	size = len(list)
	if size == 0 or size == 1:
		# Sorted.
		return list
	
	mid_i = int((size - 1) / 2)
	beg_i = 0
	end_i = size - 1
		
	elems = [list[beg_i], list[mid_i], list[end_i]]
	ssort(elems)
	
	pivot = elems[1]
	i = 0
	j = size - 1
	while True:
		while list[i] < pivot:
			i += 1

		while list[j] > pivot:
			j -= 1

		if i >= j:
			break
		elif list[i] == pivot and list[j] == pivot:
			i += 1
			j -= 1

		list[i], list[j] = list[j], list[i]
	
	assert(list[:i]   != list)
	assert(list[i+1:] != list)
	list[   :i] = qsort(list[   :i])
	list[i+1: ] = qsort(list[i+1: ])
	return list

class Item:
	key = None
	val = None

	def __init__(self, key, val):
		self.key = key
		self.val = val
	
	def __lt__(self, other):
		return self.key < other.key
	
	def __gt__(self, other):
		return self.key > other.key
	
	def __eq__(self, other):
		return self.key == other.key

class DictItems:
	items = []
	size  = 0

	def __init__(self, d):
		self.items = []
		for (key, val) in d.iteritems():
			self.items.insert(0, Item(key, val))
		
		self.size = len(self.items)
		qsort(self.items)

	def as_dict(self):
		d = {}
		for i in self.items:
			d[i.key] = i.val

		return d

def find_changes(old, new, deleted):
	i = 0
	j = 0
	o = old.items
	n = new.items

	rv = None
	while i != old.size or j != new.size:
		a = None
		b = None
		if i < old.size:
			a = o[i]
		if j < new.size:
			b = n[j]

		if j == new.size or (i != old.size and a < b):
			# Only in old
			if rv is None:
				rv = {}
			rv[a.key] = a.val

			i += 1
		elif i == old.size or (j != new.size and b < a):
			# Only in new
			if rv is None:
				rv = {}
			rv[b.key] = deleted
			
			j += 1
		elif a.val == b.val:
			# Same in both
			i += 1
			j += 1
		else:
			# Different in both
			if rv is None:
				rv = {}
			rv[a.key] = a.val

			i += 1
			j += 1

	return rv
	

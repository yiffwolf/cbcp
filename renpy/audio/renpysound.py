# Copyright 2004-2019 Tom Rothamel <pytom@bishoujo.us>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""
This file defines the API that Ren'Py uses to communicate with an audio and video
backend, and the default implementation of the API. This API is not intended
to be stable between multiple Ren'Py releases, and so is more intended for use
in ports to different platforms.

There are a few common variables with specific datatypes here.

`channel`
    An integer giving  the number of an audio channel.  These integers are
    allocated densely, but there's no limit to how many channels may be
    present at one time.

`file`
    This is an object representing an open file. This may be a Python file
    object, or a Ren'Py subfile object. All objects past to this have a
    `name` field, giving the name of the file. SubFiles also have `base`
    and length. Base is the offset from the start of the file where the data
    begins, while length is the amount of data in total.

Times and durations are represented as floats giving the number of seconds.

A channel may have up to two files associated with it, the playing file and
the queued file. The queued file begins playing when the current file ends
or is stopped.
"""


from __future__ import print_function

def check_error():
    """
    This is called by Ren'Py to check for an error. This function should raise
    a meaningful exception if an error has occurred in a background thread,
    or do nothing if an error has not occured. (It should clear any error that
    it raises.)
    """
    pass


def play(channel, file, name, paused=False, fadein=0, tight=False, start=0, end=0):
    """
    Plays `file` on `channel`. This clears the playing and queued samples and
    replaces them with this file.

    `name`
        A python object giving a readable name for the file.

    `paused`
        If True, playback is paused rather than started.

    `fadein`
        The time it should take the fade the music in.

    `tight`
        If true, the file is played in tight mode. This means that fadeouts
        can span between this file and the file queued after it.

    `start`
        A time in the file to start playing.

    `end`
        A time in the file to end playing.    `
    """
    pass


def queue(channel, file, name, fadein=0, tight=False, start=0, end=0):
    """
    Queues `file` on `channel` to play when the current file ends. If no file is
    playing, plays it.

    The other arguments are as for play.
    """
    pass


def stop(channel):
    """
    Immediately stops `channel`, and unqueues any queued audio file.
    """
    pass


def dequeue(channel, even_tight=False):
    """
    Dequeues the queued sound file.

    `even_tight`
        If true, a queued sound file that is tight is not dequeued. If false,
        a file marked as tight is dequeued.
    """
    pass


def queue_depth(channel):
    """
    Returns the queue depth of the channel. 0 if no file is playing, 1 if
    a files is playing but there is no queued file, and 2 if a file is playing
    and one is queued.
    """

    return 0

def playing_name(channel):
    """
    Returns the `name`  argument of the playing sound. This was passed into
    `play` or `queue`.
    """

    return "nut"

def pause(channel):
    """
    Pauses `channel`.
    """
    pass


def unpause(channel):
    """
    Unpauses `channel`.
    """
    pass


def unpause_all():
    """
    Unpauses all channels that are paused.
    """
    pass


def fadeout(channel, delay):
    """
    Fades out `channel` over `delay` seconds.
    """
    pass

def busy(channel):
    """
    Returns true if `channel` is currently playing something, and false
    otherwise
    """

    return False

def get_pos(channel):
    """
    Returns the position of the audio file playing in `channel`. Returns None
    if not file is is playing or it is not known.
    """

    return None

def get_duration(channel):
    """
    Reutrns the duration of the audio file playing in `channel`, or None if no
    file is playing or it is not known.
    """

    return None

def set_volume(channel, volume):
    """
    Sets the primary volume for `channel` to `volume`, a number between
    0 and 1. This volume control is perceptual, taking into account the
    logarithmic nature of human hearing.
    """
    pass

def set_pan(channel, pan, delay):
    """
    Sets the pan for channel.

    `pan`
        A number between -1 and 1 that control the placement of the audio.
        If this is -1, then all audio is sent to the left channel.
        If it's 0, then the two channels are equally balanced. If it's 1,
        then all audio is sent to the right ear.

    `delay`
        The amount of time it takes for the panning to occur.
    """
    pass


def set_secondary_volume(channel, volume, delay):
    """
    Sets the secondary volume for channel. This is linear, and is multiplied
    with the primary volume and scale factors derived from pan to find the
    actual multiplier used on the samples.

    `delay`
        The time it takes for the change in volume to happen.
    """
    pass


def get_volume(channel):
    """
    Gets the primary volume associated with `channel`.
    """

    return 1

def video_ready(channel):
    """
    Returns true if the video playing on `channel` has a frame ready for
    presentation.
    """

    return False

def read_video(channel):
    """
    Returns the frame of video playing on `channel`. This should be returned
    as an SDL surface with 2px of padding on all sides.
    """
    return None


# No video will be played from this channel.
NO_VIDEO = 0

# The video will be played while avoiding framedrops.
NODROP_VIDEO = 1

# The video will be played, allowing framedrops.
DROP_VIDEO = 2

def set_video(channel, video):
    """
    Sets a flag that determines if this channel will attempt to decode video.
    """
    pass

def init(freq, stereo, samples, status=False, equal_mono=False):
    """
    Initializes the audio system with the given parameters. The parameter are
    just informational - the audio system should be able to play all supported
    files.

    `freq`
        The sample frequency to play back at.

    `stereo`
        Should we play in stereo (generally true).

    `samples`
        The length of the sample buffer.

    `status`
        If true, the sound system will print errors out to the console.
    `
    """
    pass


def quit(): # @ReservedAssignment
    """
    De-initializes the audio system.
    """
    pass


def periodic():
    """
    Called periodically (at 20 Hz).
    """
    pass


def advance_time():
    """
    Called to advance time at the start of a frame.
    """
    pass

# Store the sample surfaces so they stay alive.
rgb_surface = None
rgba_surface = None

def sample_surfaces(rgb, rgba):
    """
    Called to provide sample surfaces to the display system. The surfaces
    returned by read_video should be in the same format as these.
    """

    global rgb_surface
    global rgba_surface

    rgb_surface = rgb
    rgba_surface = rgb



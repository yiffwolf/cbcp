#!/usr/bin/python2

import renpy
import cPickle as cp
import json

import cbcp.rpyutils
from cbcp.rpyutils import bootstrap,Rpc2Reader,RpaReader

def __task__():
	bootstrap()

	import os
	for root, dirs, files in os.walk(cbcp.WORK_DIR, topdown=False):
		for fname in files:
			if fname.endswith(".rpyc") or fname.endswith(".rpymc"):
				print("\t* extracting keys from {}".format(fname))
				rpc = Rpc2Reader(os.path.join(root, fname))

				sections = {}
				for (number, offset, size) in rpc.slots():
					if number == 0:
						print("\t\t=> Blank section")
						continue
					elif not number in [1, 2, 3]:
						print("\t\t=> Invalid section")
						continue
					
					(data, _) = cp.loads(rpc.slot(number))
					sections[number] = data

					print("\t\t=> ID: {}\tOffset: 0x{:x}\tSize: {}".format(number, offset, size))
					print("\t\t   Ver: {}\tKey: {}".format(data["version"], data["key"]))
				
				kfname = fname + ".json"
				kfpath = os.path.join(os.path.join(cbcp.KEYS_DIR, root), kfname)
				kfdirn = os.path.dirname(kfpath)
				
				if not os.path.exists(kfdirn):
					os.makedirs(kfdirn)

				f = open(kfpath, "w")
				f.write(json.dumps(sections))
				f.close()
	
	for root, dirs, files in os.walk(cbcp.SOURCE_DIR, topdown=False):
		for fname in files:
			if fname.endswith(".rpa") or fname.endswith(".rpi"):
				print("\t* extracting archive data from {}".format(fname))
				rpa = RpaReader.new(os.path.join(root, fname))

				# Extract key parameters
				params = {}
				params["version"] = rpa.version()
				params["offset"]  = rpa.offset()
				params["key"]     = rpa.key()
				params["index"]   = rpa.index()
				
				print("\t\t=> Ver: {}\t\tIndex Offset: 0x{:x}".format(params["version"], params["offset"]))
				print("\t\t   Key: 0x{:x}\tIndex Count:  {}".format(params["key"], len(params["index"])))

				kfname = fname + ".json"
				kfpath = os.path.join(os.path.join(cbcp.KEYS_DIR, root), kfname)
				kfdirn = os.path.dirname(kfpath)

				if not os.path.exists(kfdirn):
					os.makedirs(kfdirn)

				f = open(kfpath, "w")
				f.write(json.dumps(params))
				f.close()
	return (True, "")

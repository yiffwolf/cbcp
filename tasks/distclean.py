#/usr/bin/env python3
import os
import cbcp

def __task__():
	for root, dirs, files in os.walk(cbcp.WORK_DIR, topdown=False):
		for fname in files:
			if fname.endswith(".rpyc") or fname.endswith(".pyc") or fname.endswith(".rpymc"):
				print("\t* removing {}".format(fname))
				os.remove(os.path.join(root, fname))
	return (True, "")

#/usr/bin/env python3
# encoding=utf-8

import os
import fd4
import cbcp
import cbcp.tools
import multiprocessing
import concurrent.futures as futures
import bsdiff4

def diff(a, b, writer, batch=67108860):
	a.seek(0, 0)
	b.seek(0, 0)

	from threading import Lock
	l0 = Lock()
	l1 = Lock()

	def process():
		while True:
			l0.acquire()
			aoff = a.tell()
			boff = b.tell()
			abuf = a.read(batch)
			bbuf = b.read(batch)
			l0.release()

			alen = len(abuf)
			blen = len(bbuf)

			if alen == 0 and blen == 0:
				break

			print("\t\t=> diffing {} byte section at 0x{:x} with {} byte section at 0x{:x}".format(alen, aoff, blen, boff))
			diff = bsdiff4.diff(abuf, bbuf)
			l1.acquire()
			writer.push_fragment(alen, diff)
			l1.release()

	cpus  = int(1) # Limited due to memory constraints
	work  = futures.ThreadPoolExecutor(max_workers=cpus)
	queue = []
	for i in range(cpus):
		queue.append(work.submit(process))

	# Wait for all of the workers to finish
	for worker in queue:
		worker.result()

	writer.commit()

def __task__():
	ddirn = cbcp.DIFF_DIR
	cbcp.tools.ensure_dirs([ddirn])

	sdirn = cbcp.SOURCE_DIR
	tdirn = cbcp.TARGET_DIR
	if not os.path.exists(sdirn) or not os.path.exists(tdirn):
		print("\t\t* there's nothing to do.")
		return

	# Match each file in src/ to a file in target/ and diff them
	for root, dirs, files in os.walk(sdirn):
		for fname in files:
			src_name = os.path.join(root, fname)
			tgt_name = os.path.join(root.replace(sdirn, tdirn), fname)
			dif_name = os.path.join(root.replace(sdirn, ddirn), fname) + ".rp4"

			if not os.access(src_name, os.R_OK):
				print("\t\t! cannot open source file {}".format(src_name))
				continue

			if not os.access(tgt_name, os.R_OK):
				print("\t\t! cannot open target file {}".format(tgt_name))
				continue

			#if not os.access(dif_name, os.W_OK):
			#	print("\t\t! cannot open patch file {} for writing".format(dif_name))
			#	continue

			print("\t\t* diff {} {} => {}".format(src_name, tgt_name, dif_name))

			src = open(src_name, "rb")
			tgt = open(tgt_name, "rb")

			dif = fd4.Fd4Writer(dif_name)
			diff(src, tgt, dif)
			dif.close()

#!/usr/bin/env python2
# coding=utf-8
#
# Ren'Py RPC2 (.rpyc) pickler.
# Author: Yoichi Fan <yme@waifu.club>
#

import hashlib
import cPickle as cp
import json
import renpy

import cbcp
import cbcp.rpyutils as rpyutils
from cbcp.rpyutils import Rpc2Writer


def build(root, fname):
	import os
	src_name = os.path.join(root, fname)
	tgt_name = src_name + "c"

	print("\t* compiling {}".format(src_name))
	ast = renpy.parser.parse(src_name)
	if ast == None:
		raise Exception("Failed to compile {}".format(fname))

	# Assign missing names to the nodes
	nodes = []
	for node in ast:
		node.get_children(nodes.append)
		
	import time
	ver = int(time.time())
	ser = 0
	for node in nodes:
		if node.name is None:
			node.name = (src_name.replace(cbcp.WORK_DIR, ""), ver, ser)
			ser += 1

	# Start writing the file
	rpc = Rpc2Writer(tgt_name)
	rpc.write_header()

	# Lookup keys in the key database
	kfname = fname + "c.json"
	kfpath = os.path.join(os.path.join(cbcp.KEYS_DIR, root), kfname)
	keys   = {}
	try:
		if os.access(kfpath, os.R_OK):
			f    = open(kfpath, "r")
			s    = f.read()
			keys = json.loads(s)
			f.close()
	except Exception as e:
		print(e)

	for i in [1, 2]:
		d = {}
		if str(i) in keys:
			d = keys[str(i)]

		if not "key" in d:
			print("\t\t=> No key found, assumming 'unlocked'")
			d["key"] = "unlocked"

		if not "version" in d:
			print("\t\t=> No version found, assuming 5003000")
			d["version"] = 5003000

		# Here the Ren'Py script compiler would pre-translate
		# We, instead, just write the same data twice
		dump = cp.dumps((d, ast), 2)	
		rpc.write_slot(i, dump)
	
	tgt_file = open(tgt_name, "rU")
	digest   = hashlib.md5(tgt_file.read()).digest()
	tgt_file.close()
	
	rpc.write_md5(digest)

def __task__():
	rpyutils.bootstrap()

	import os
	import os.path
	
	jobs = []
	for root, dirs, files in os.walk(cbcp.WORK_DIR, topdown=False):
		for fname in files:
			if fname.endswith(".rpy") or fname.endswith(".rpym"):
				src_name = os.path.join(root, fname)
				tgt_name = src_name + "c"
				if os.access(tgt_name, os.R_OK | os.W_OK):
					# See if this file needs updating
					if os.path.getmtime(tgt_name) > os.path.getmtime(src_name):
						# The target is already up to date
						continue
				
				jobs.append((root, fname))

	if len(jobs) == 0:
		print("\t* targets are up to date")
		return True

	# As per the design of the engine, lots of the language's statements get
	# registered and defined by python evals inside language files.
	# Here we load those files.
	rpyutils.load_commons()
	
	threads = None
	lock    = None
	def pull_jobs():
		while True:
			lock.acquire()

			if len(jobs) == 0:
				lock.release()
				break
			
			(root, fname) = jobs.pop()
			lock.release()
			
			build(root, fname)

	
	if cbcp.PARALLEL:
		from threading import Thread,Lock

		threads = []
		lock    = Lock()
		for i in range(cbcp.JOBS):
			threads.append(Thread(target=pull_jobs))

		for worker in threads:
			worker.start()

		for worker in threads:
			worker.join()
	else:
		for (root, fname) in jobs:
			build(root, fname)
	
	return (True, "")

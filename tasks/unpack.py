#/usr/bin/env python3
import os
import cbcp
import cbcp.tools
from cbcp.rpyutils import RpaReader

def unpack(fname):
	afname = os.path.join(cbcp.SOURCE_DIR, fname)
	twdirn = os.path.join(cbcp.WORK_DIR, fname)

	if not os.access(afname, os.R_OK):
		print("\t\t! cannot find {}".format(afname))
		return (False, "cannot find {}".format(afname))	

	print("\t\t* unpacking {}".format(afname))
	ar = RpaReader.new(afname)

	cbcp.tools.ensure_dirs([twdirn])
	
	i = 1
	for key in ar.index().keys():
		tfname = os.path.join(twdirn, key)
		cbcp.tools.ensure_dirs([os.path.dirname(tfname)])

		sf = ar.open(key)
		tf = open(tfname, "wb")

		name = key
		if len(name) > cbcp.NAMELEN:
			name  = name[:cbcp.NAMELEN - 3]
			name += "..."
		name = name.ljust(cbcp.NAMELEN)

		print("\r\t\t=> [{}/{}]\t{}".format(i, len(ar.index().keys()), name), end="", flush=True)

		cbcp.tools.fcopy(sf, tf)
		tf.close()

		i += 1

	ar.close()
	
	print("")
	return (True, "")

def __task__():
	print("")
	for root, dirs, files in os.walk(cbcp.SOURCE_DIR):
		for fname in files:
			if fname.endswith(".rpa"):
				fname = os.path.join(root.replace(cbcp.SOURCE_DIR, ""), fname)
				unpack(fname)

	return (True, "")

#!/usr/bin/env python3
# coding=utf-8
import os
import json

import cbcp
import cbcp.tools
from cbcp.rpyutils import Rpa3Writer

def repack(src_fname):
	sfname = os.path.join(cbcp.SOURCE_DIR, src_fname)
	tfname = os.path.join(cbcp.TARGET_DIR, src_fname)
	kfname = os.path.join(cbcp.KEYS_DIR,   sfname) + ".json"
	wdirn  = os.path.join(cbcp.WORK_DIR,   src_fname) + "/"

	if os.path.exists(wdirn):
		# Try to load the key data of the RPA so we can rebuild it
		k = {}
		if os.access(kfname, os.R_OK):
			kf = open(kfname, "r")
			k  = json.loads(kf.read())
			kf.close()

			if not k["version"][0] == 3:
				print("\t\t! file {} requires unsupported version {}".format(src_fname, k["version"]))
				return False

		print("\t\t* repackaging file {}".format(src_fname))

		t = Rpa3Writer(tfname, mode="w")
		if "key" in k:
			t.set_key(k["key"])

		fragments = []
		index     = {}
		# Queue up all of the fragments we know from the key file
		if "index" in k:
			index = k["index"]
			for (key, frags) in index.items():
				for frag in frags:
					if len(frag) == 2:
						frag = (frag[0], frag[1], None)

					fragments.append((key, 0, frag))

			# Sort fragments by the original offset, so we preserve
			# the order the fragments were originally written in
			fragments.sort(key=lambda frag: frag[2][0])

		# Queue any new files
		for root, dirs, files in os.walk(wdirn):
			for fname in files:
				key = os.path.join(root.replace(wdirn, ""), fname)
				key = key.replace("\\", "/")
				if key in index:
					continue

				fpath = os.path.join(root, fname)
				if not os.access(fpath, os.R_OK):
					print("\t\t! no read access to file {}".format(fname))
					continue

				print("\t\t=> queuing previously unseen file {}".format(key))

				f = open(fpath, "rb")
				f.seek(0, 2)
				fragments.append((key, 0, (None, f.tell(), "")))
				f.close()

		# Drop any files that can't be found
		for key in t.index().keys():
			sfname = key
			sfpath = os.path.join(wdirn, sfname)
			if not os.access(sfpath, os.R_OK):
				print("\t\t! file {} was not found".format(sfpath))
				if t.in_index(key):
					print("\t\t\t=> dropping from archive")
					t.delete(key)

		# A fragment is in either the (offset, length, start) format.
		# If start is None, it must be removed from the tuple or
		# replaced with the empty string. If the offset is None, it
		# doesn't care where it gets put.
		counter  = 1
		for frag in fragments:
			(key, skip, frag) = frag
			sfname = key
			sfpath = os.path.join(wdirn, sfname)

			if not os.access(sfpath, os.R_OK):
				continue

			sf = open(sfpath, "rb")
			sf.seek(0, 2)
			sflen  = sf.tell()
			offset = frag[0]
			length = frag[1]
			start  = frag[2]
			roff   = t.get_file().tell()

			print("\t\t=> [{}/{}]\t{}".format(counter, len(fragments), key))
			print("\t\t   writing {}/{} remaining (of {}) bytes to 0x{:x}".format(length, sflen - skip, sflen, roff), end="")

			sf.seek(skip, 0)
			t.push_fragment(sf, key, length, offset, start=None)
			sf.close()

			roff   = t.get_file().tell()
			delta  = None
			if not offset is None:
				delta = roff - offset - length

			if delta is None:
				print(" (No expected offset)")
			else:
				print(" ({} offset delta)".format(delta))

			# If this is not the entire file, queue up another fragment so
			# we can finish writing the entire thing after the end
			if skip + length < sflen:
				l = sflen - skip - length
				s = length + skip
				fragments.append((key, s, (None, l, start)))
			counter += 1

		t.commit()
		return True

def update(src_fname):
	sfname = os.path.join(cbcp.SOURCE_DIR, src_fname)
	tfname = os.path.join(cbcp.TARGET_DIR, src_fname)
	kfname = os.path.join(cbcp.KEYS_DIR,   sfname) + ".json"
	wdirn  = os.path.join(cbcp.WORK_DIR,   src_fname) + "/"
	tfstat = os.stat(tfname)

	if os.path.exists(wdirn):
		print("\t\t* updating file {}".format(src_fname))
		t = Rpa3Writer(tfname, mode="a")

		keys  = []
		index = t.index()
		queue = []
		for key in index.keys():
			ffname = os.path.join(wdirn, key)
			if not os.access(ffname, os.R_OK):
				queue.append(key)
				continue

			ffstat = os.stat(ffname)
			if cbcp.tools.stat_newer(tfstat, ffstat):
				continue

			print("\t\t=> querying updated file {}".format(ffname))
			keys.append(key.replace("\\", "/"))

		for key in queue:
			print("\t\t=> removing innaccessible file {}".format(ffname))
			t.delete(key.replace("\\", "/"))

		# Queue any new files
		for root, dirs, files in os.walk(wdirn):
			for fname in files:
				key = os.path.join(root.replace(wdirn, ""), fname)
				key = key.replace("\\", "/")
				if key in index:
					continue

				fpath = os.path.join(root, fname)
				if not os.access(fpath, os.R_OK):
					print("\t\t! no read access to file {}".format(fname))
					continue

				print("\t\t=> queuing previously unseen file {}".format(key))

				keys.append(key)

		# A fragment is in either the (offset, length, start) format.
		# If start is None, it must be removed from the tuple or
		# replaced with the empty string. If the offset is None, it
		# doesn't care where it gets put.
		counter = 1
		for key in keys:
			sfname = key
			sfpath = os.path.join(wdirn, sfname)

			if not os.access(sfpath, os.R_OK):
				print("\t\t! file {} was not found".format(sfpath))
				continue

			sf = open(sfpath, "rb")
			sf.seek(0, 2)
			sflen  = sf.tell()
			sf.seek(0, 0)
			ts = t.open(key)
			tslen = ts.length()

			name = key
			if len(name) > cbcp.NAMELEN:
				name  = name[:cbcp.NAMELEN - 3]
				name += "..."
			name = name.ljust(cbcp.NAMELEN)

			print("\r\t\t=> [{}/{}]\t{}".format(counter, len(keys), key), end="", flush=True)
			written = cbcp.tools.fcopy(sf, ts)

			if tslen > written:
				t.shrink(key, written)

		print("")
		t.commit()

		orphans = t.get_orphans()
		total = 0
		for orphan in orphans:
			total += orphan[1]

		if len(orphans) > 0:
			print("\t\t! leaked {} orphans wasting {} bytes".format(len(orphans), total))

		return True

def __task__():
	cbcp.tools.ensure_std_dirs()

	for root, dirs, files in os.walk(cbcp.SOURCE_DIR):
		for fname in files:
			if fname.endswith(".rpa"):
				fname  = os.path.join(root.replace(cbcp.SOURCE_DIR, ""), fname)
				tfname = os.path.join(cbcp.TARGET_DIR, fname)

				failed = False
				if os.access(tfname, os.R_OK | os.W_OK):
					try:
						update(fname)
					except Exception as ex:
						raise ex
						print(ex)
						print("\t\t! failed to update, rebuilding")
						failed = True

				else:
					repack(fname)

				if failed:
					repack(fname)

#!/usr/bin/env python3
from cbcp.rpyutils import RpaReader

def main():
	from sys import argv,exit
	if len(argv) < 2:
		print("Usage: {} <filename>".format(argv[0]))
		exit(1)
	
	if argv[1] == "-k":
		if not len(argv) > 4:
			print("Usage: {} -k <key> <filename>".format(argv[0]))
			exit(1)

		key  = argv[2]
		mode = "key"
		fstart = 3
	else:
		mode = "print"
		fstart = 1

	for fname in argv[fstart:]:
		print(fname)
		ar = RpaReader.new(fname)

		print("\tkey\t{}".format(ar.key()))
		print("\tver\t{}".format(ar.version()))
		
		if mode == "print":
			print("\tindex")
			for entry in ar.index().keys():
				print("\t- {}".format(entry))
		elif mode == "key":
			if key in ar.index():
				print("\tfile: {}".format(key))
				for frag in ar.index()[key]:
					print("\t\t{}".format(frag))
			else:
				print("\tno such file: {}".format(key))

		print("")
		print("")
		

if __name__ == "__main__":
	main()

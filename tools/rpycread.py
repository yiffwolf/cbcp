#!/usr/bin/env python3
# encoding=utf-8
import zlib
import struct
class Rpc2:
	HEADER="RENPY RPC2".encode("us-ascii")
	def __init__(self, fn):
		self.file = open(fn, "rb")

	def slots(self):
		f = self.file
		l = len(struct.pack("III", 0, 0, 0))
		
		f.seek(len(self.HEADER), 0)
		b = f.read(l * 3)

		s = (struct.unpack("III", b[0:l]), struct.unpack("III", b[l:l*2]), struct.unpack("III", b[2*l:3*l]))
		return s

	def slot(self, n):
		if not n in [1, 2, 3]:
			raise IndexError("Only sections 1, 2 and 3 are supported")

		f = self.file
		for (number, offset, size) in self.slots():
			if number == n:
				f.seek(offset, 0)
				b = f.read(size)
				return zlib.decompress(b)
		
		return None

def main():
	from sys import argv, stdout, exit
	if not len(argv) in [2, 3]:
		print("Usage: {} <file> [slot]".format(argv[0]))
		exit(1)
	
	r = Rpc2(argv[1])
	if len(argv) == 2:
		print("Slot layout for file {}:".format(argv[1]))
		for (number, offset, size) in r.slots():
			if number == 0:
				print("\t=> Blank Slot")
				continue
			print("\t=> ID {}, Offset 0x{:x}, Size {}".format(number, offset, size))
	elif len(argv) == 3:
		stdout.buffer.write(r.slot(int(argv[2])))
		stdout.buffer.flush()
		

if __name__ == "__main__":
	main()

#!/usr/bin/env python3
import cbcp
import cbcp.boot
from cbcp.boot import summon,SUMMON_SELF

if __name__ == "__main__":
	print("!!! SUMMONING TASKS DIRECTLY IS A DEVELOPER FEATURE")
	print("!!! MAKE SURE YOU KNOW WHAT YOU'RE DOING")

	from sys import argv, stdout, stderr
	if len(argv) == 2:
		print("- Summoning {} on {}:".format(argv[1], SUMMON_SELF))
		summon(SUMMON_SELF, argv[1], "__task__", stdout=stdout, stderr=stderr)
	elif len(argv) > 2:
		print("- Summoning {} on {}:".format(argv[2], argv[1]))
		summon(argv[1], argv[2], "__task__", stdout=stdout, stderr=stderr)
	else:
		print("Usage: {} [python] <task>".format(argv[0]))
